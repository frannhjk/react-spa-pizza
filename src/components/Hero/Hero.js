import React, { useState }from 'react'
import Navbar from '../Navbar/Navbar'
import { HeroContainer, HeroContent, HeroItems, HeroH1, HeroP, HeroBtn } from './HeroElements'
import Sidebar from '../Sidebar/Sidebar';


const Hero = () => {
    const [isOpen, setIsOpen] = useState(false);

    const toggleFunction = () => {
        setIsOpen(!isOpen)
    }

    return (
        <HeroContainer>
            <Navbar toggle={toggleFunction}/>
            <Sidebar isOpen={isOpen} toggle={toggleFunction}/>
            <HeroContent>
                <HeroItems>
                    <HeroH1>Kiosco Pizza</HeroH1>
                    <HeroP>Consulta nuestras variedades y envianos un pedido!</HeroP>
                    <HeroBtn>Ordenar!</HeroBtn>
                </HeroItems>
            </HeroContent>
        </HeroContainer>
    )
}

export default Hero;