import React from 'react';
import { FeatureContainer, FeatureButton } from './FeatureElements';


const Feature = () => {
    return (
        <FeatureContainer>
            <h1>Pizza del dia</h1>
            <p>Fugazzeta rellena con queso mozarela y jamon cocido.</p>
            <FeatureButton>Ordenar</FeatureButton>
        </FeatureContainer>
    )
}

export default Feature;