import product1 from '../../images/product-1.jpg';
import product2 from '../../images/product-2.jpg';
import product3 from '../../images/product-3.jpg';
import empanda1 from '../../images/empanada1.jpg';
import empanda2 from '../../images/empanada2.jpg';
import empanda3 from '../../images/empanada3.jpg';


export const ProductsData = [
    {
        img: product1,
        alt: 'Pizza', 
        name: 'Pizza Suprema',
        desc: 'Descripcion',
        price: '$300.00',
        button: 'Comprar'
    },
    {
        img: product2,
        alt: 'Pizza', 
        name: 'Pizza Hawaiana',
        desc: 'Descripcion',
        price: '$280.00',
        button: 'Comprar'
    },
    {
        img: product3,
        alt: 'Pizza', 
        name: 'Pizza Veggie Style',
        desc: 'Descripcion',
        price: '$250.00',
        button: 'Comprar'
    }
]

export const ProductsDataTwo = [
    {
        img: empanda1,
        alt: 'Empanada', 
        name: 'Empanada de carne',
        desc: 'Descripcion',
        price: '$40.00',
        button: 'Comprar'
    },
    {
        img: empanda2,
        alt: 'Empanada', 
        name: 'Empanada de jamon y queso',
        desc: 'Descripcion',
        price: '$40.00',
        button: 'Comprar'
    },
    {
        img: empanda3,
        alt: 'Empanada', 
        name: 'Empanada de pollo',
        desc: 'Descripcion',
        price: '$40.00',
        button: 'Comprar'
    }
]