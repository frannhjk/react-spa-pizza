import React from 'react';
import { ProductsContainer, ProductsHeading, ProductsWrapper, ProductCard, ProductImg, 
    ProductInfo, ProductTitle, ProductDesc, ProductPrice, ProductBtn } from './ProductElements';

const Product = ({ heading, productsData}) => {
    return (
        <ProductsContainer>
            <ProductsHeading>
                {heading}
            </ProductsHeading>
            <ProductsWrapper>
                {productsData.map((product, index) => {
                    return (
                        <ProductCard key={index}>
                            <ProductImg src={product.img} alt={product.alt} />
                            <ProductInfo>
                                <ProductTitle>{product.name}</ProductTitle>
                                <ProductDesc>{product.desc}</ProductDesc>
                                <ProductPrice>{product.price}</ProductPrice>
                                <ProductBtn>{product.button}</ProductBtn>
                            </ProductInfo>
                         </ProductCard>
                    )
                })}
            </ProductsWrapper>
        </ProductsContainer>
    )
}   

export default Product;
