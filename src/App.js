import React from 'react'
import { BrowserRouter, Router, Switch } from 'react-router-dom'
import { GlobalStyle } from './globalStyles'

import { ProductsData, ProductsDataTwo} from '../src/components/Products/ProductsData';


import Hero from '../src/components/Hero/Hero'
import Product from '../src/components/Products/Product'
import Feature from '../src/components/Feature/Feature'
import Footer from '../src/components/Footer/Footer'

function App() {
  return (
    <BrowserRouter>
      <GlobalStyle /> 
      <Hero />
      <Product heading='Elegi tu favorita' productsData={ProductsData}/>
      <Feature />
      <Product heading='Elegi tu favorita' productsData={ProductsDataTwo}/>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
